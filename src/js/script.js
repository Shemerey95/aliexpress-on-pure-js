"use strict";

import {getCookie} from './modules';

document.addEventListener('DOMContentLoaded', () => {
    const search              = document.querySelector('.search');
    // const searchInput    = document.getElementById('searchGoods');
    const cartButton          = document.getElementById('cart');
    const wishListButton      = document.getElementById('wishlist');
    const goodsWrapper        = document.querySelector('.goods-wrapper');
    const cart                = document.querySelector('.cart');
    const basketConfirmButton = document.querySelector('.cart-confirm');
    const category            = document.querySelector('.category');
    const wishListCounter     = wishListButton.querySelector('.counter');
    const cartCounter         = cartButton.querySelector('.counter');
    const cartWrapper         = cart.querySelector('.cart-wrapper');
    const basketTotalPrice    = cart.querySelector('.cart-total > span');

    const wishList    = [];
    const goodsBasket = {};

    // Вывод спиннера во время загрузки товаров
    const spinnerRenderCard = (spinner, handlerFunction) => {
        const spinnerBlock = `
            <div id="spinner">
                <h3>Загрузка..</h3>
                <div class="spinner-loading">
                    <div><div><div></div></div><div><div></div></div>
                    <div><div></div></div><div><div></div></div></div>
                </div>
            </div>        
        `;

        spinner ?
            goodsWrapper.insertAdjacentHTML('beforeend', spinnerBlock) :
            handlerFunction !== showGoodsRemoveBasket ?
                cartWrapper.insertAdjacentHTML('beforeend', spinnerBlock) :
                0;
    };

    // переход на главнуб страницу
    const goToIndexPage = () => setTimeout(() => {
        document.location.href = '/';
    }, 3000);

    // получение списка товаров из "БД"
    const getGoods = (handler, filter, spinner = true) => {
        spinnerRenderCard(spinner, handler);

        fetch('/src/db/db.json')
            .then(response => response.json())
            .then(filter)
            .then(handler);
    };

    // вывод и добавление товара на страницу
    const creatCardGood = (id, title, price, img) => {
        const cardGood = document.createElement('div');

        cardGood.className = 'card-wrapper col-12 col-md-6 col-lg-4 col-xl-3 pb-3';

        let goodContent = `<div class="card">
                                <div class="card-img-wrapper">
                                    <img class="card-img-top" src="/src/${img}" alt="">
                                    <button class="card-add-wishlist ${wishList.includes(id) ? 'active' : ''}"
                                        data-goods-id="${id}"></button>
                                </div>
                                <div class="card-body justify-content-between">
                                    <a href="#" class="card-title">${title}</a>
                                    <div class="card-price">${price} ₽</div>
                                    <div>
                                        <button class="card-add-cart" data-goods-id="${id}">Добавить в корзину</button>
                                    </div>
                                </div>
                            </div>
        `;

        cardGood.insertAdjacentHTML('beforeend', goodContent);

        return cardGood;
    };

    // рендер товаров в корзине
    // вывод и добавление товара на странице корзины
    const creatCardBasket = (id, title, price, img) => {
        const cartGood = document.createElement('div');

        cartGood.className = 'goods';

        let goodContent = `<div class="goods-img-wrapper">
                                <img class="goods-img" src="/src/${img}" alt="${title}">
            
                            </div>
                            <div class="goods-description">
                                <h2 class="goods-title">${title}</h2>
                                <p class="goods-price">${price} ₽</p>
            
                            </div>
                            <div class="goods-price-count">
                                <div class="goods-trigger">
                                    <button class="goods-add-wishlist ${wishList.includes(id) ? 'active' : ''}"
                                        data-goods-id="${id}"></button>
                                    <button class="goods-delete" data-goods-id="${id}"></button>
                                </div>
                                <div class="goods-count">
                                    <span id="goods__deleteGoodFromBasket" class="goods__currentCountBasket" data-goods-id="${id}">-</span>
                                        <span>${goodsBasket[id]}</span>
                                    <span id="goods__addGoodToBasket" class="goods__currentCountBasket" data-goods-id="${id}">+</span>
                                </div>
                            </div>
        `;

        cartGood.insertAdjacentHTML('beforeend', goodContent);

        return cartGood;
    };

    // вывод товаров на страницу поэлементно
    const renderCard = items => {
        goodsWrapper.textContent = '';

        if (items.length) {
            items.forEach(({id, title, price, imgMin}) => {
                goodsWrapper.insertAdjacentElement('beforeend', creatCardGood(id, title, price, imgMin));
            });
        } else {
            goodsWrapper.insertAdjacentHTML('beforeend', `<h3>😱 Товаров по Вашему запросу не найдено! 😢</h3>`);

            goToIndexPage();
        }
    };

    // добавленире товаров на страницу корзины
    const renderBasket = items => {
        cartWrapper.textContent = '';

        if (items.length) {
            items.forEach(({id, title, price, imgMin}) => {
                // cartWrapper.append(creatCardBasket(id, title, price, imgMin));
                cartWrapper.insertAdjacentElement('beforeend', creatCardBasket(id, title, price, imgMin));
            });
        } else {
            cartWrapper.insertAdjacentHTML('beforeend', `<div id="cart-empty">Ваша корзина пока пуста</div>`);
        }
    };

    // вызов подсчета итоговой цены в корзине
    const showGoodsRemoveBasket = goods => {
        const basketGoods = goods.filter(good => goodsBasket.hasOwnProperty(good.id));

        calcTotalPrice(basketGoods);
    };

    // расчет и вывод итоговой цены товаров в корзине
    const calcTotalPrice = goods => {
        const totalPrice = goods.reduce((accumulator, good) => {
            return accumulator + good.price * goodsBasket[good.id];
        }, 0);

        basketTotalPrice.textContent = totalPrice.toFixed(1);
    };

    // вывод количества изранных товаров
    const checkCountWishList = () => {
        wishListCounter.textContent = wishList.length.toString();
    };

    // вывод количества товаров в корзине
    const checkCountGoodsBasket = () => {
        cartCounter.textContent = Object.getOwnPropertyNames(goodsBasket).length.toString();
    };

    // фильтрация по товарам корзины
    const showGoodsBasket = goods => {
        const basketGoods = goods.filter(good => goodsBasket.hasOwnProperty(good.id));

        calcTotalPrice(basketGoods);

        return basketGoods;
    };

    // рандомный вывод товаров (типа фильтр)
    const randomSort = items => items.sort(() => Math.random() - 0.5);

    //фильтр по категориям
    const selectCategory = event => {
        event.preventDefault();

        const target = event.target;

        if (target.classList.contains('category-item')) {
            const category = target.dataset.category;

            getGoods(renderCard,
                goods => goods.filter((item) => item.category.includes(category)));
        }
    };

    //работа с хранилищами
    // записываем в куки товары в корзине
    const cookieQuery = get => {
        if (get) {
            if (getCookie('goodsBasket')) {
                Object.assign(goodsBasket, JSON.parse(getCookie('goodsBasket')));
            }
            checkCountGoodsBasket();
        } else {
            document.cookie = `goodsBasket=${JSON.stringify(goodsBasket)}; max-age=86400e3;`
        }
    };

    // сохранение и получение избранных товаров в local storage
    const storageQueryGoods = get => {
        if (get) {
            if (localStorage.getItem('wishListGoods')) {
                // wishList.splice(Infinity, 0, ...JSON.parse(localStorage.getItem('wishListGoods')));
                wishList.push(...JSON.parse(localStorage.getItem('wishListGoods')));

                checkCountWishList();
            }
        } else {
            localStorage.setItem('wishListGoods', JSON.stringify(wishList));
        }
    };

    //события
    // закрытие корзины
    const closeBasket = event => {
        const targetEvent = event.target;

        if (targetEvent === cart || targetEvent.classList.contains('cart-close') || event.code === 'Escape') {
            cart.style.display = 'none';
            document.removeEventListener('keyup', closeBasket);
        }
    };

    //открытие корзины
    const openBasket = event => {
        event.preventDefault();
        cart.style.display = 'flex';
        document.addEventListener('keyup', closeBasket);

        getGoods(renderBasket, showGoodsBasket, false);
    };

    // // моментальный поиск по товарам
    // const searchGoods = event => {
    //     const target            = event.target;
    //     const lowerSearchString = target.value.toLowerCase();
    //     if (lowerSearchString.length > 0) {
    //         getGoods(renderCard,
    //             goods => goods.filter((item) =>
    //                 item.title.toLowerCase().includes(lowerSearchString) ||
    //                 item.price.toString().toLowerCase().includes(lowerSearchString)
    //             ));
    //     }
    // };

    // поиск по товарам
    const searchGoods = event => {
        event.preventDefault();

        const input      = event.target.elements.searchGoods;
        const inputValue = input.value.trim();

        if (inputValue !== '') {
            // const searchString = new RegExp(inputValue, 'i');

            getGoods(renderCard,
                goods => goods.filter((item) =>
                    // searchString.test(item.title) ||
                    // searchString.test(item.price)

                    item.title.toLowerCase().includes(inputValue.toLowerCase()) ||
                    item.price.toString().toLowerCase().includes(inputValue.toLowerCase())
                ));
        } else {
            search.classList.add('error');
            setTimeout(() => {
                search.classList.remove('error');
            }, 2000);
        }

        input.value = '';
    };

    // Добавление и удаление товаров из избанного
    const toggleWishList = (goodId, currentGood) => {
        if (wishList.includes(goodId)) {
            const indexGoodId = wishList.indexOf(goodId);

            wishList.splice(indexGoodId, 1);
            currentGood.classList.remove('active');
        } else {
            wishList.push(goodId);
            currentGood.classList.add('active');
        }

        storageQueryGoods();
        checkCountWishList();
    };

    // добавление товаров в корзину
    const addGoodBasket = (goodId) => {
        if (goodsBasket[goodId]) {
            goodsBasket[goodId] += 1;
        } else {
            goodsBasket[goodId] = 1;

            checkCountGoodsBasket();
        }

        cookieQuery();
    };

    // удаление товаров из корзины (полностью)
    const deleteGoodsBasket = (goodId, item) => {
        if (confirm('Товар будет удален из корзины. Продолжить?')) {
            item.closest('.goods').remove();
            delete goodsBasket[goodId];

            checkCountGoodsBasket();

            if (Object.keys(goodsBasket).length) {
                getGoods(showGoodsRemoveBasket, 1, false);
            } else {
                cartWrapper.insertAdjacentHTML('beforeend', `<div id="cart-empty">Ваша корзина пока пуста</div>`);
                basketTotalPrice.textContent = '0.0';
            }

            cookieQuery();
        }
    };

    // открытие списка избранных товаров
    const showWishList = () => {
        getGoods(renderCard,
            goods => goods.filter((good) => wishList.includes(good.id)),
            false);
    };

    // "Якобы" отправка заказа из корзины на сервер
    const basketConfirm = () => {
        Object.keys(goodsBasket).forEach(key => delete goodsBasket[key]);

        checkCountGoodsBasket();
        cookieQuery();
        getGoods(renderBasket, showGoodsBasket, false);
    };

    // уменьшение количнства выбранного товара в корзине
    const decreaseCurrentGood = (goodId, item) => {
        if (goodsBasket[goodId] !== 1) {
            goodsBasket[goodId] -= 1;

            item.nextElementSibling.textContent = goodsBasket[goodId];
        } else {
            deleteGoodsBasket(goodId, item);
        }

        getGoods(showGoodsRemoveBasket, 1, false);
        cookieQuery();
    };

    // увеличение количнства выбранного товара в корзине
    const increaseCurrentGood = (goodId, countGood) => {
        goodsBasket[goodId] += 1;

        getGoods(showGoodsRemoveBasket, 1, false);

        countGood.textContent = goodsBasket[goodId];

        cookieQuery();
    };

    // Обработка десйтвий с товарами (добавление/удаление в избранное/корзину)
    const handlerGoods = event => {
        const target  = event.target;
        const goodsId = target.dataset.goodsId;

        if (target.classList.contains('card-add-wishlist')) toggleWishList(goodsId, target);
        if (target.classList.contains('card-add-cart')) addGoodBasket(goodsId);
        if (target.classList.contains('goods-add-wishlist')) toggleWishList(goodsId, target);
        if (target.classList.contains('goods-delete')) deleteGoodsBasket(goodsId, target);
        if (target.getAttribute('id') === 'goods__deleteGoodFromBasket') decreaseCurrentGood(goodsId, target);
        if (target.getAttribute('id') === 'goods__addGoodToBasket') increaseCurrentGood(goodsId, target.previousElementSibling);
    };

    // инициализация
    {
        getGoods(renderCard, randomSort);
        cookieQuery(true);
        storageQueryGoods(true);

        // вызовы обработчиков событий
        cartButton.addEventListener('click', openBasket);
        cart.addEventListener('click', closeBasket);
        category.addEventListener('click', selectCategory);
        // searchInput.addEventListener('input', searchGoods);
        search.addEventListener('submit', searchGoods);
        goodsWrapper.addEventListener('click', handlerGoods);
        wishListButton.addEventListener('click', showWishList);
        cart.addEventListener('click', handlerGoods);
        basketConfirmButton.addEventListener('click', basketConfirm);
    }
});
